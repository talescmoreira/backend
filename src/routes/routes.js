const { Router } = require('express');
const MediaController = require('../controllers/MediaController');
const ReviewController = require('../controllers/ReviewController');
const Media = require('../models/Media');


const router = Router();

router.post('/medias/:new', MediaController.create);
router.get('/medias', MediaController.index);
router.get('/medias/:id', MediaController.show);
router.patch('/medias/:update', MediaController.update);
router.delete('/medias/:destroy', MediaController.destroy);
router.post('/medias/:addRelation', MediaController.addRelation);
router.delete('/medias/:removeRelation', MediaController.removeRelation);



router.post('/reviews/:new', ReviewController.create);
router.get('/reviews', ReviewController.index);
router.get('/reviews/:id', ReviewController.show);
router.patch('/reviews/:update', ReviewController.update);
router.delete('/reviews/:destroy', ReviewController.destroy);
router.post('/reviews/:addRelation', ReviewController.addRelation);
router.delete('/reviews/:removeRelation', ReviewController.removeRelation);


module.exports = router;