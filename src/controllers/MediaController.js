const { response } = require('express');
const Media = require('../models/Media');
const Review = require('../models/Review');

const create = async(req,res) => {
    try{
          const media = await Media.create(req.body);
          return res.status(201).json({message: "Usuário cadastrado com sucesso!", media: media});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const media = await Media.findAll();
        return res.status(200).json({media});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const media = await Media.findByPk(id);
        return res.status(200).json({media});
    }catch(err){
        return res.status(500).json({err});
    }
};


const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Media.update(req.body, {where: {id: id}});
        if(updated) {
            const media = await Media.findByPk(id);
            return res.status(200).send(media);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Media.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

const addRelation = async(req,res) => {
    const {id} = req.params;
    try {
        const review = await Review.findByPk(id);
        const media = await Media.findByPk(req.body.id);
        await review.setRole(media);
        return res.status(200).json(review);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRelation = async(req,res) => {
    const {id} = req.params;
    try {
        const media = await Media.findByPk(id);
        await media.setRole(null);
        return res.status(200).json(media);
    }catch(err){
        return res.status(500).json({err});
    }
}




module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addRelation,
    removeRelation
};
