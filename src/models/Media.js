const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Media = sequelize.define('Media', {
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },

    evaluation: {
        type: DataTypes.FLOAT,
        allowNull: false
    },

    release_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    reviews: {
        type: DataTypes.STRING,
        allowNull: false
    },

    genre: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false
});

Media.associate = function(models) {
    Media.hasMany(models.Review);
}

module.exports = Media;