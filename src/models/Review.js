const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Review = sequelize.define('Review', {
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },

    user_id: {
        type: DataTypes.STRING,
        allowNull: false
    },

    text: {
        type: DataTypes.STRING,
        allowNull: false
    },

    likes: {
        type: DataTypes.INTEGER
    },

    deslikes: {
        type: DataTypes.INTEGER
    },

    response: {
        type: DataTypes.STRING
    }
}, {

});

Review.associate = function(models) {
    Review.belongsTo(models.Media, { });
}

module.exports = Review;